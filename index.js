/*  Section Function */


// Parameter and Arguments
/* function printInput() {
  let nickname = prompt('Enter your nickname: ');
  console.log(`Hi, ${nickname}` );
}

printInput(); */


// This function has parameter and argument
function printName(name) { //name --> parameter
  console.log(`My name is ${name}`);
}
printName('Juan'); 
printName('Reynan Delizo');  //  "Juana" --> Argument

// 1. printName("Juana"); >> ARGUMENT
// 2. will be stored to our PARAMETER >> function printName(name)
// 3. The information/data stored in a Parameter can be used to the code block inside a function.

let sampleVariable = "Yui";


printName(sampleVariable);  //  "Juana" --> Argument

// Function arguments cannot be used by a function if there are no parameters provided within the function

function checkDivisibilityBy8(num) {
  let remainder = num % 8; // modulus use to check the remaider of number given output

  console.log(`the remainder of ${num} divideed by 8 is: ${remainder}`);

  let isDivisibleBy8 = remainder === 0;
  console.log(`Is ${num} divisible by 8? ${isDivisibleBy8}`);

}
checkDivisibilityBy8(64);

// You can also do the sama using(), however, take note that prompt() outputs a string, String are not ideal for mathematical computations.

// Function parameters can also accept other functions as arguments
// Some complex function use other as arguments to perform more complicated results
//  This will be further seen we discuss array methods

function argumentFunction() {
  console.log('This function was passed as an argument before the message.');
}


function invokeFunction(argumentFunction) {
    argumentFunction()
}


invokeFunction(argumentFunction);

console.log(argumentFunction);


// Multiple parameters
function createFullName(firstName, middleName ,lastName) {
  console.log(firstName + " " + middleName + " " + lastName)
}

createFullName('Reynan', 'Soriano', 'Delizo');


// Multiple parameter using stored data in a variable
let firstName = "John";
let middleName ="Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);


// Section Return Statement

function returnFullName(firstName, middleName, lastName) {
   return firstName + ' ' + middleName  + ' ' + lastName;
}

let completeName = returnFullName("juan", "Dela", "Cruz");
console.log(completeName);

function returnAddress(city, country) {
  let fullAddress = city + " ," + country;

  return fullAddress;
}

returnAddress();
